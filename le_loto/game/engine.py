import random
import time
from typing import Dict

from colorama import Fore, Style

from .card import CardType, Card
from .player import Player, BotDifficulty, Bot, PLAYERS_NAME, Human
from ..exceptions import IncorrectInputError
from ..utils import console_clear


class Engine:

    def __init__(self, with_console_clear: bool = True, with_color: bool = True, skip_bots: bool = False):
        """
        :param with_console_clear: clear console after every new number, default True
        :type with_console_clear: bool
        :param with_color: use colors in the console with colorama, default: True
        :type with_color: bool
        :param skip_bots: do not display bot responses, default: False
        :type skip_bots: bool
        """
        self._players: Dict[str, Player] = dict()
        self._active_players: Dict[str, Player] = dict()
        self._losers: Dict[str, Player] = dict()
        self._default_card_type: CardType = CardType.LOTTO

        self.with_colorama: bool = with_color
        self.with_console_clear: bool = with_console_clear
        self.skip_bots: bool = skip_bots

        self.__MAIN_MENU_ACTIONS = {0: self.continue_,
                                    1: self.new_game,
                                    2: self.settings,
                                    3: self.game_stat,
                                    4: exit}

        self.__SETTINGS_ACTIONS = {1: lambda: setattr(self, 'with_console_clear', not self.with_console_clear),
                                   2: lambda: setattr(self, 'with_colorama', not self.with_colorama),
                                   3: lambda: setattr(self, 'skip_bots', not self.skip_bots),
                                   4: self.select_card_type,
                                   5: self.append_players,
                                   6: self.main_menu}

    def main_menu(self):
        """
        From the main menu, you can start new game, continue or customize the game. And also see statistics
        """
        continue_ = len(self._players) != 0
        self.__colored_print(Fore.CYAN, 'Welcome to le-Loto game')
        self.__print_many_dashes()
        additional_menu = '0. Continue\n' if continue_ else ''
        print(f"{additional_menu}"
              f"1. New game\n"
              f"2. Settings\n"
              f"3. Statistics\n"
              f"-------\n"
              f"4. Exit")

        user_choice, err = self.__user_input_handler(self.__MAIN_MENU_ACTIONS if continue_ else {1, 2, 3, 4})
        if not err:
            self.__MAIN_MENU_ACTIONS.get(user_choice, 4)()
        self.main_menu()

    def settings(self):
        """
        Game settings menu
        1. "Clear the console after round" - clear console after every new number
        2. "Use colors in the console" - use colors in the console with colorama
        3. "Skip bots" - do not display bot responses
        4. "Select card type" - use one of three possible types of cards:
                                        - lotto card;
                                        - card with random numbers;
                                        - card with random numbers, but in ascending order;
        5. "Add players" - call the menu for adding players
        """
        self.__print_many_dashes()

        print("1. Clear the console after round: {}\n"
              "2. Use colors in the console: {}\n"
              "3. Skip bots: {}\n"
              "4. Select card type. Default: {}\n"
              "5. Add players\n"
              "-------\n"
              "6. Back to main menu".format(self.with_console_clear,
                                            self.with_colorama,
                                            self.skip_bots,
                                            self._default_card_type.name))

        user_choice, err = self.__user_input_handler(self.__SETTINGS_ACTIONS)
        if not err:
            self.__SETTINGS_ACTIONS.get(user_choice, 6)()
        self.settings()

    def append_players(self):
        """
        Append players menu
        Used to add human players, EASY bots, NORMAL bots, and HARD bots
        """
        self.__print_many_dashes()
        self.__colored_print(Fore.CYAN, 'Now we will configure the number of players and bots')

        print('Enter the number of people (0-100)')
        user_choice, err = self.__user_input_handler(set(range(101)))
        if not err:
            self.__append_humans(user_choice)

        for difficulty in tuple(BotDifficulty):
            self.__colored_print(Fore.YELLOW, f'Enter the number of {difficulty.name} bots (0-100)')
            user_choice, err = self.__user_input_handler(set(range(101)))
            if not err:
                self.__append_bots(difficulty, user_choice)

    def new_game(self):
        """
        Reset saved data and start a new game
        """
        self.select_card_type()
        self._players.clear()
        self.append_players()
        self._active_players.update(self._players)
        self.__colored_print(Fore.CYAN, 'Init new game for {} players:'.format(len(self._active_players)))
        players = '\n'.join(('{}. {}'.format(i + 1, name) for i, name in enumerate(self._active_players)))
        self.__colored_print(Fore.CYAN, players)
        self.playing_game()
        self.game_over()
        self.game_stat()

    def continue_(self):
        """
        Continue the game with past players and do not reset statistics
        """
        self._active_players.update(self._players)
        self._losers.clear()
        for player in self._active_players.values():
            player.new_card()
        self.__colored_print(Fore.CYAN, 'Continue game for {} players:'.format(len(self._active_players)))
        self.playing_game()
        self.game_over()
        self.game_stat()

    def playing_game(self):
        """
        Playing game menu
        As long as there are active players and numbers in the pool, the game continues.
        Get a number from the pool, in a cycle each player makes a choice
        """
        self.__console_clear()
        self.__print_many_dashes()
        self.__colored_print(Style.BRIGHT, "You Think It's Over Just Because I Am Dead,\n"
                                           "But The Games Have Just Begun.")
        self.__press_enter_to_continue()
        pool = self.get_nums_pool()
        while pool and self._active_players:
            num = pool.pop()
            for name in list(self._active_players):
                player = self._active_players[name]

                skip_bot = self.skip_bots and player.is_bot
                if not skip_bot:
                    self.__print_many_dashes()
                    self.__colored_print(Fore.YELLOW if player.is_bot else '', str(player))
                    self.__colored_print(Fore.BLUE, 'Current number: {}'.format(num))
                    player_choice = player.cross_out_current_num(num)
                else:
                    player_choice = player.making_choice(num)

                player_is_right = player_choice == player.is_num_in_card(num)
                self.__crossed_out(player, player_is_right)

                if skip_bot:
                    continue

                time.sleep(0.5)
            self.__console_clear()

    def game_over(self):
        """
        At the end of the game, each player who gets here will receive +1 in stat of wins.
        If there are winners, there will be a message with a list of the winner
        Else: "No winners, all players lost"
        """
        if not self._active_players:
            self.__colored_print(Fore.CYAN, 'No winners, all players lost')
        else:
            for player in self._active_players.values():
                player.win()
            self.__colored_print(Fore.CYAN, 'Congratulations!\nWinners: {}'.format(', '.join(self._active_players)))

    def game_stat(self):
        """
        Displays game statistics, wins, losses and win rate of all players
        """
        self.__print_many_dashes()
        self.__colored_print(Fore.YELLOW + Style.BRIGHT, 'Statistics\n')
        if not self._players:
            self.__colored_print(Fore.CYAN, 'There are 0 players in the game')

        for name, player in self._players.items():
            self.__colored_print(Fore.CYAN, '{} — wins: {}, losses: {}. Win rate: {:.2f}%'.format(
                name, player.stat.win, player.stat.lose, player.stat.percentage_of_wins()))
        self.__print_many_dashes()

    def select_card_type(self):
        """
        The menu prompts to choose one of 3 types of cards
        1. Standard lotto card, where the first column is 1-9, the second is 10-19
        2. Card with random numbers in each column
        3. Card with random numbers sorted in ascending order ->
        """
        self.__print_many_dashes()
        self.__colored_print(Fore.CYAN, 'Select card type')
        print('1. Standard lotto card, where the first column is 1-9, the second is 10-19\n'
              '2. Card with random numbers in each column\n'
              '3. Card with random numbers sorted in ascending order ->')
        user_choice, err = self.__user_input_handler({1, 2, 3})
        if not err:
            self._default_card_type = CardType(user_choice)
        else:
            self.__colored_print(Fore.RED, 'Mistake in choosing. Default card type is used: {}'.format(
                self._default_card_type.name))

    def __append_humans(self, count: int, players_name: list = PLAYERS_NAME):
        for i in range(count):
            name = players_name.pop() if players_name else 'Player {}'.format(len(self._players))
            player = Human(name, Card(self._default_card_type))
            self._players[player.name] = player

    def __append_bots(self, difficulty: BotDifficulty, count: int):
        for i in range(count):
            name = '{} {}'.format(difficulty.name, len(self._players))
            player = Bot(name, Card(self._default_card_type), difficulty)
            self._players[player.name] = player

    def __crossed_out(self, player: Player, is_correct: bool):
        """
        If the number is crossed out incorrectly, the player loses.
        Loss message print, loss statistics update.
        If not a bot, print please "press enter to continue"
        """
        if not is_correct:
            if not (self.skip_bots and player.is_bot):
                self.__colored_print(Fore.RED, 'Sorry, but you lose "{}"'.format(player.name))
            if not player.is_bot:
                self.__press_enter_to_continue()
            player.lose()
            del self._active_players[player.name]
            self._losers[player.name] = player

    def __colored_print(self, color: str, text: str, end: str = '\n', with_reset: bool = True):
        """
        If color console is on, color output, otherwise classic
        :param color: colorama color, for example Fore.GREEN
        :type color: str
        :param text: printed text
        :type text: str
        :param end: string appended after the last value, default a newline
        :type end: str
        :param with_reset: clear color after print, Style.RESET_ALL
        :type with_reset: bool
        """
        reset = Style.RESET_ALL if with_reset else ''
        print(color + text + reset if self.with_colorama else text, end=end)

    def __print_many_dashes(self):
        self.__colored_print(Fore.GREEN, '-' * 64)

    def __press_enter_to_continue(self):
        self.__colored_print(Fore.GREEN, 'Press Enter to continue...')
        input()

    def __user_input_handler(self, possible_nums) -> (int, bool):
        """
        User keyboard input and validation
        :return: user choice, False if not err else True
        :rtype: (int, bool)
        """
        self.__colored_print(Fore.CYAN, 'Your choice:', end=' ')
        try:
            user_choice = int(input())
            if user_choice not in possible_nums:
                raise IncorrectInputError
            return user_choice, False
        except (ValueError, IncorrectInputError):
            self.__colored_print(Fore.RED, 'Incorrect input')
            return -1, True

    def __console_clear(self):
        if self.with_console_clear:
            console_clear()

    @staticmethod
    def get_nums_pool() -> list:
        """
        Shuffled pool of numbers from 1 to 90 inclusive
        """
        pool = list(range(1, 91))
        random.shuffle(pool)
        return pool
