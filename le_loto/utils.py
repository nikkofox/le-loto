import os


def console_clear():
    os.system('cls' if os.name in {'nt', 'dos'} else 'clear')
