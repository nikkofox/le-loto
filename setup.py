import setuptools

with open("rules.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="le-Loto",
    version="1.0.0",
    author="Artyom Sviridov",
    description="Let's play almost lotto",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/NikkoFox/le-loto",
    packages=setuptools.find_packages(exclude=['tests']),
    include_package_data=True,
    python_requires=">=3.7",
    install_requires=[
        "colorama>=0.4.4"
    ],
    entry_points="""
    [console_scripts]
    le-loto=le_loto.cli_loto:main
    """
)
