from le_loto.cli_loto import get_args


def test_cli_input():
    argv = get_args(['-s', '-c', '-S'])
    assert argv.clear_console is True
    assert argv.simple_console is False
    assert argv.skip_bots is True
