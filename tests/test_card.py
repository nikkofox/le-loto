import pytest

from le_loto.game.card import CardType, Card


@pytest.fixture
def card():
    card = Card()
    yield card


@pytest.fixture(params=[1, 2, 3])
def card_type(request):
    yield CardType(request.param)


class TestCard:
    def test_init(self, card_type):
        card = Card(card_type)
        assert card.card_type is CardType(card_type.value)

    def test_create(self, card):
        card.create()
        assert len(card.card) == 3 and len(card) == 15

    def test_create_lotto(self, card):
        card.create_lotto()
        assert card.card_type is CardType.LOTTO
        card_nums = (x for x in card.card[0] if isinstance(x, int))
        assert next(card_nums) < next(card_nums) < next(card_nums)

    def test_create_random(self, card):
        card.create_random()
        assert card.card_type is CardType.RANDOM

    def test_create_increasing_random(self, card):
        card.create_increasing_random()
        assert card.card_type is CardType.INCREASING_RANDOM
        card_nums = (x for x in card.card[0] if isinstance(x, int))
        assert next(card_nums) < next(card_nums) < next(card_nums)

    def test_contains(self, card):
        card.create()
        num = next(x for x in card.card[0] if isinstance(x, int))
        assert num in card

    def test_cross_out_num(self, card):
        card.create()
        num_index = next(i for i in range(len(card.card[0])) if isinstance(card.card[0][i], int))
        assert card.cross_out(card.card[0][num_index]) is True
        assert card.cross_out(card.card[0][num_index]) is False
        assert card.card[0][num_index] == '--'
        assert card.cross_out(999) is False
