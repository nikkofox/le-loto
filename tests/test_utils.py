from unittest import mock

from le_loto.utils import console_clear


@mock.patch("os.system")
def test_console_clear(mocked_os_system):
    console_clear()
    mocked_os_system.assert_called_once()
