# le-Loto

[![Hits-of-Code](https://hitsofcode.com/gitlab/NikkoFox/le-loto)](https://hitsofcode.com/gitlab/NikkoFox/le-loto/view)

## Задача

Реализовать аналог игры лото.    
[Подробные правила тут](rules.md).

## Three ways to start

1. * `pip install .` then just type `le-loto`
1. * `python -m le_loto`
1. * `docker build -t le-loto . && docker run -it le-loto`

***

![](https://i.imgur.com/zY3yHn6.gif)
